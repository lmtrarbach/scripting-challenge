#!/usr/bin/python
#Desenvolvido por Rafael Tomelin e Luthiano Trarbach @Interop
import subprocess
import os.path
from ruamel.yaml import YAML
import re
import time
yaml = YAML()#Load the yaml
def openYaml(pipelinepath):#This function open the pipeline file and parse like dados
    try:
        if os.path.isfile(pipelinepath):#Check no existence of the file
            #jenkins_version= subprocess.check_output(["java", "-jar", "/usr/lib/jenkins/jenkins.war", "--version"])
            #jenkins_version=jenkins_version.strip("\n")
            with open("pipeline.yml", 'r') as stream: # Open yml files as stream
                try:
                    dados=(yaml.load(stream))# Load the yaml and return as dados
                    return dados
                except:
                    return "Error getting yamlOpen" # Report a error in yaml open

        else:
            print "Theres no way to the file"#Can't reach the file

    except:
        print "unknown error" # Report a error exception
def branch():#This function  does the checkout of git branch to master

    command=dados['branch']
    print command
    subprocess.call("git checkout " + command, shell=True)
def workerTasks(step):# This function take a step of pipeline dict and parse to subprocess to execute
    step=str(step)#Does a string sanitize
    for y in  dados['tasks']:#iterate dados['tasks'] for tasks

        try:

            command= (y[step]['cmd'])#create a var with sthe stepname and the commnad for parsing
            print command
            subprocess.call(command, shell=True)#Run the command
        except:
            print "Theres a trouble in subprocess" #Report a error in case of failures




dados=openYaml("./pipeline.yml")#Use the function for parsing the pipeline for the var dados
def main():
    try:

        branch()#execuute the branch checkout
        time.sleep(1)
        print "build"
        workerTasks('build')#Execute the build step
        time.sleep(1)
        print "release: build"
        workerTasks('build') #Execute the build step for the release steps
        time.sleep(1)
        print "release: integration"
        workerTasks('integration')#Execute the integration step for the release steps
        time.sleep(1)
        print "release: compress"
        workerTasks('compress')#Execute the compress step for the release steps
        print "Sucess"
    except:
        print "Error to execute the pipeline"
main()
